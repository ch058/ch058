#include<stdio.h>

struct date
{
    int d;
    int m;
    int y;
};

struct Employee
{
    int emp_id;
    char emp_name[25];
    float emp_salary;
    struct date emp_joining_date;
};

int main()
{
    int n;
    printf("Enter the number of employees");
    scanf("%d",&n);
    struct Employee E[n];
    for(int i=0;i<n;i++)
    {
        printf("\nEnter Employee's Id : ");
        scanf("%d",&E[i].emp_id);
        printf("\nEnter Employee's Name : ");
        scanf("%s"E[i].emp_name);
        printf("\nEnter Employee's Salary : ");
        scanf("%f",&E[i].emp_salary);
        printf("\nEnter Employee's date of joining(ddmmyy) : ");
        scanf("%d%d%d",&E[i].emp_joining_date.d,&E[i].emp_joining_date.m,&E[i].emp_joining_date.y);
    }
    for(int i=0;i<n;i++)
    {
        printf("\nDetails of Employees");
        printf("\nEmployee's Id : %d",E[i].emp_id);
        printf("\nEmployee's Name : %s",E[i].emp_name);
        printf("\nEmployee's Salary : %f",E[i].emp_salary);
        printf("\nEmployee's date of joining  : %d-%d-%d",E[i].emp_joining_date.d,E[i].emp_joining_date.m,E[i].emp_joining_date.y);
    }
    return 0;
}