#include <stdio.h> 
  
int search(int arr[], int n, int x) 
{ 
    int i; 
    for (i = 0; i < n; i++) 
        if (arr[i] == x) 
            return i;
    return -1;
}        
  
int main() 
{   int n,x;
    printf("Enter size of array: ");
    scanf("%d",&n);
    int arr[n];
    printf("Enter the values: ");
    for(int i = 0; i < 5; ++i) 
    {
        scanf("%d", &arr[i]); 
    }    
    printf("Enter the element to be searched: ");
    scanf("%d",&x);
    int result = search(arr, n, x); 
    if(result==-1)
    {
        printf("The element is not in the array.");
    }
    else 
    {
        printf("The element is at %d",result+1);
    }
    return 0; 
}