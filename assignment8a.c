#include <stdio.h>

void input(float *arr,int n)
{
    for(int i=0;i<n;i++)
    {
        printf("Enter a number : ");
        scanf("%f",&arr[i]);
    }
}

void printArray(float *arr, int n)
{
    printf("Printing the array :\n");
    for(int i=0;i<n;i++)
    {
        printf("%f  ",arr[i]);
    }
}

int main()
{
    int n;
    printf("Enter the length of the array : ");
    scanf("%d",&n);
    float arr[n];
    input(arr,n);
    printArray(arr,n);
    return 0;
}
