#include <stdio.h>

int delete(int arr[], int n, int x) 
{
	int i; 
   	for (i=0; i<n; i++) 
    	if (arr[i] == x) 
			if (i < n) 
   			{ 
     			n = n - 1; 
     			for (int j=i; j<n; j++) 
        		arr[j] = arr[j+1]; 
   			}
	return n; 
} 
  
int main() 
{ 
    int arr[] = {2,4,5,6,8,10},x; 
    int n = sizeof(arr)/sizeof(arr[0]); 
    printf("Array before deletion\n"); 
    for (int i = 0; i < n; i++) 
    {
    	printf("%d  ", arr[i]);  
    }
    printf("\nThe element to be deleted(5)\n");
    scanf("%d",&x);
    n = delete(arr, n, x); 
	printf("\nModified array is \n"); 
    for (int i=0; i<n; i++) 
    	printf("%d ",arr[i]); 
	return 0; 
}