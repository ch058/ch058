#include<stdio.h>

void input(float *a)
{
    printf("\nEnter first number : ");
    scanf("%f", &a[1]);
    printf("\nEnter second number : ");
    scanf("%f", &a[2]);
    
}

float sum(float a, float b)
{
    float c = a+b;
    return c;
}
void print(float a)
{
    printf("The number is : %f",a);
}

int main()
{
	printf("Enter two numbers to be added\n\n");
	float a[2];
	input(a);
	float c = sum(a[1],a[2]);
	printf("\nFirst number: \n");
	print(a[1]);
	printf("\nSecond number: \n");
	print(a[2]);
	printf("\nSum: \n");
	print(c);
	return 0;
}