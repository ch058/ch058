//Program to find area of a circle

#include <stdio.h>

#define pi 3.141

void input(float *r)
{
    float a ;
    printf("\nEnter the radius of the circle\n");
    scanf("%f",&a);
    *r = a;
}

float areacalc(float r)
{
    float area = pi * r * r ;
    return area;
}

void output(float area)
{
    printf("\n\nThe area of the circle is %f\n\n", area);
}

int main()
{
    float r;
    input(&r);
    float area = areacalc(r);
    output(area);
    return 0;
}