#include<stdio.h>

int inputhr()
{
    printf("Enter hours : ");
    int n;
    scanf("%d",&n);
    return n;
}

int inputmin()
{
    printf("Enter minutes : ");
    int n;
    scanf("%d",&n);
    return n;
}

int absMin(int hr , int min)
{
    int absmin = 60*hr + min;;
    return absmin;
}

int main()
{
    int hr = inputhr();
    int min = inputmin();
    int absmin = absMin(hr,min);
    printf("\n\nTime in minutes is %d", absmin);
    return 0;
}