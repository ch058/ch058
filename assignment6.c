#include<stdio.h>

int input()
{
    int n;
    printf("Enter a number : ");
    scanf("%d",&n);
    return n;
}

int numReverse(int n)
{
    int rev = 0;
    while(n/10 !=0)
    {
        rev +=n%10;
        rev *=10;
        n /=10;
    }
    rev += n;
    return rev;
}

int palindromeCheck(int n)
{
    if(n == numReverse(n))
        return 1;
    else
        return 0;
}

int main()
{
    int n = input();
    if(palindromeCheck(n))
        printf("\nThe number is a palindrome.");
    else
        printf("\nThe number is not a palindrome.");
    return 0;
}