#include <stdio.h>

void input(float *arr,int n)
{
    for(int i=0;i<n;i++)
    {
        printf("Enter a number : ");
        scanf("%f",&arr[i]);
    }
}

void ic(float *arr, int n)
{
    int minPos = 0;
    int maxPos = 0;
    for(int i=1;i<n;i++)
    {
        if(arr[i]<arr[minPos])
            minPos=i;
        if(arr[i]>arr[maxPos])
            maxPos=i;
    }
    float temp = arr[maxPos];
    arr[maxPos]=arr[minPos];
    arr[minPos]=temp;
}

void printArray(float *arr, int n)
{
    printf("Printing the array :\n");
    for(int i=0;i<n;i++)
    {
        printf("%f  ",arr[i]);
    }
}

int main()
{
    int n;
    printf("Enter the length of the array : ");
    scanf("%d",&n);
    float arr[n];
    input(arr,n);
    ic(arr,n);
    printArray(arr,n);
    return 0;
}
