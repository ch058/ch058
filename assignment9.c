#include <stdio.h>
int main()
{
    int arr[100],beg,mid,end,n,i,x;
    printf("Enter the number of elements : ");
    scanf("%d",&n);
    for(i=0;i<n;i++)
    {
        printf("Enter the elements : ");
        scanf("%d",&arr[i]);
    }
    printf("Enter the element to be found : ");
    scanf("%d",&x);
    beg=0;
    end=n-1;

    while(beg<=end)
    { 
        mid = (beg+end)/2;
        if(arr[mid]<x)
        {
            beg = mid+1;
        }
        else if(arr[mid]==x)
        {
            printf("It is at position %d ",mid);
            break;
        }
        else if(arr[mid]>x)
        {
            end=mid-1;
        }
    }
    if(beg>end)
    {
        printf("The value isn't found here ");
    }
    return 0;
}