#include<stdio.h>

int input()
{
    int n;
    printf("Enter a number");
    scanf("%d",&n);
    return n;
}

void multipleCalc(int *multiples , int n)
{
    multiples[0] = n;
    for(int i = 1; i < 100/n; i++)
    {
        multiples[i] = multiples[i-1] + n;
    }
}

void printMultiples(int *multiples , int n)
{
    for(int i=0 ; i < 100/n ; i++)
        printf("%d\n",multiples[i]);
}

int main()
{
    int n = input();
    int multiples[100/n];
    multipleCalc(multiples,n);
    printMultiples(multiples,n);
    return 0;
}