#include <stdio.h>
float add(float *n1, float *n2)
{
    return *n1 + *n2;
}
float sub(float *n1, float *n2)
{
    return *n1 - *n2;
}
float multi(float *n1, float *n2)
{
    return *n1 * *n2;
}
float divide(float *n1, float *n2)
{
    return *n1 / *n2;
}


int main()
{
    float n1, n2, r;
    printf("Enter the numbers\n");
    scanf("%f%f",&n1,&n2);
	
    r = add(&n1, &n2);
    printf("\n%f + %f = %f", n1, n2, r);
    r = sub(&n1, &n2);
    printf("\n%f - %f = %f", n1, n2, r);
    r = multi(&n1, &n2);
    printf("\n%f * %f = %f", n1, n2, r);
    r = divide (&n1, &n2);
    printf("\n%f / %f = %f", n1, n2, r);

    return 0;
}