#include <stdio.h>

struct fraction
{
    int numerator, denominator;
};

void input(struct fraction *a)
{
    printf("\nEnter numerator : ");
    scanf("%d",&a->numerator);
    printf("\nEnter denominator : ");
    scanf("%d",&a->denominator);
}

int gcfCalc(int a, int b)
{
    int diff=a;
    while(a!=b)
    {
        if(a>b)
        {
            a-=b;
            diff=a;
        }
        else
        {
            b-=a;
            diff=b;
        }
    }
    return diff;
}

struct fraction addFrac(struct fraction x, struct fraction y)
{
    struct fraction sum;
    sum.numerator = x.numerator*y.denominator + y.numerator*x.denominator;
    sum.denominator=x.denominator*y.denominator;
    int gcf = gcfCalc(sum.numerator,sum.denominator);
    sum.numerator/=gcf;
    sum.denominator/=gcf;
    return sum;
}

void output(struct fraction a, struct fraction b, struct fraction sum)
{
    printf("\nThe sum of fractions    %d/%d    and    %d/%d    is    %d/%d\n",a.numerator,a.denominator,b.numerator,b.denominator,sum.numerator,sum.denominator);
}

int main()
{
    struct fraction a,b,sum;
    printf("\nEnter details for fraction 1 : \n");
    input(&a);
    printf("\nEnter details for fraction 2 : \n");
    input(&b);
    sum = addFrac(a,b);
    output(a,b,sum);
    return 0;
}