#include<stdio.h>

int input()
{
    int n;
    printf("Enter a number");
    scanf("%d",&n);
    return n;
}

int digitSum(int n)
{
    int sum = 0;
    while(n/10 != 0)
    {
        sum += n%10;
        n /= 10;
    }
    sum+=n;
    return sum;
}

void printSum(int n)
{
    int sum = digitSum(n);
    printf("\n\nThe sum of digits is %d",sum);
}

int main()
{
    int n = input();
    printSum(n);
    return 0;
}