#include<stdio.h>
int year;
void input()
{
	printf("Enter the year\n\n");
	scanf("%d",&year);
}

void leapcheck()
{
	if((year%4==0 && year%100!=0) || year%1600==0)
		printf("\nThe year is a leap year.\n");
	else 
		printf("\nThe year is not a leap year\n");
}

int main()
{
	input();
	leapcheck();
	return 0;
}