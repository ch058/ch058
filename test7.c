#include <stdio.h>

struct student
{
    int roll_no;
    char name[40];
    char dob[20];
    int marks;
};

struct student input()
{
    struct student c;
    printf("\nEnter the name of the student : ");
    scanf("%s",c.name);
    printf("\nEnter the date of birth of the student : ");
    scanf("%s",c.dob);
    printf("\nEnter the roll number : ");
    scanf("%d",&c.roll_no);
    printf("\nEnter the marks scored : ");
    scanf("%d",&c.marks);
    return c;
}

void print(struct student a)
{
    printf("\nName of the student : ");
    printf("%s",a.name);
    printf("\nRoll number : ");
    printf("%d",a.roll_no);
    printf("\nDate of birth : ");
    printf("%s",a.dob);
    printf("\nScored marks : ");
    printf("%d",a.marks);
}

void compareMarks(struct student a, struct student b)
{
    if (a.marks>b.marks)
        printf("\n\nThe first student, %s scored more marks, which is %d",a.name,a.marks);
    else if (a.marks<b.marks)
        printf("\n\nThe second student, %s scored more marks, which is %d",b.name,b.marks);
    else
        printf("\n\nBoth students, %s and %s scored equal marks, %d",a.name,b.name,a.marks);
}

int main()
{
    struct student a,b;
    printf("\n\nEnter the details of the first student :\n\n");
    a=input();
    printf("\n\nEnter the details of the second student :\n\n");
    b=input();
    printf("\n\nPrinting the details of the first student : \n\n");
    print(a);
    printf("\n\nPrinting the details of the second student : \n\n");
    print(b);
    compareMarks(a,b);
    return 0;
}