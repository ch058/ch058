//Program to find the roots of a quadratic equation given the coefficients

#include <stdio.h>
#include <math.h>

typedef struct complex
{
    float real;
    float imaginary;
} complex ;

typedef struct quadeq
{
    float a;
    float b;
    float c;
    complex root;
} quadeq;

void input(quadeq *n)
{
    float a,b,c;
    printf("\nEnter the values of the coefficients of x^2 , x and the constant\n");
    scanf("%f%f%f",&a,&b,&c);
    n->a = a;
    n->b = b;
    n->c = c;
}

void rootcalc(quadeq *n)
{
    complex root;
    root.real = -(n->b)/2*n->a;
    if((pow((n->b),2) >= 4*n->a*n->c))
    root.imaginary = sqrt(pow((n->b),2) - 4*n->a*n->c)/2*n->a;
    else if((pow((n->b),2) < 4*n->a*n->c))
    root.imaginary = sqrt(-pow((n->b),2) + 4*n->a*n->c)/2*n->a;
    n->root = root;
}

void output(quadeq n)
{
    if((pow((n->b),2) >= 4*n->a*n->c))
    printf("\nThe roots of the equation are %f and %f \n\n", n.root.real - n.root.imaginary ,n.root.real + n.root.imaginary);
    else if((pow((n->b),2) < 4*n->a*n->c))
    printf("\nThe roots of the equation are %f + %f i and %f - %f i\n\n", n.root.real, n.root.imaginary ,n.root.real, n.root.imaginary);
}

int main()
{
    quadeq p;
    input(&p);
    rootcalc(&p);
    output(p);
    return 0;
}