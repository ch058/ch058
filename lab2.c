#include <stdio.h>
#include <math.h>

typedef struct point
{
    float x;
    float y;
} point;

void input(point *p)
{
    float a ,b ;
    printf("\nEnter the x and y coordinates of the point\n");
    scanf("%f%f",&a,&b);
    p->x = a;
    p->y = b;
}

float distcalc(point a, point b)
{
    int dist;
    dist = sqrt(pow((a.x-b.x),2)+pow((a.y-b.y),2));
    return dist;
}

void output(float dist)
{
    printf("\nThe distance between the points is %f \n\n", dist);
}

int main()
{
    float dist;
    point a , b ;
    input(&a);
    input(&b);
    output(dist);
    return 0;
}