#include<stdio.h>

int input()
{
    int n;
    printf("Enter a number");
    scanf("%d",&n);
    return n;
}

int primeCheck(int n)
{
    int ch = 0;
    for(int i = 2; i < n/2; i++)
        if(n%i == 0)
        {
            ch = 1;
            break;
        }
    if (ch)
        return 0;
    else
        return 1;
}

int main()
{
    int n = input();
    if(n == 1)
        printf("The number entered is one.");
    else if (primeCheck(n))
        printf("The number entered is prime.");
    else
        printf("The number entered is composite.");
    return 0;
}