#include <stdio.h>
int main()
{
    int i,j,r,c;
    printf("\n\nTranspose of a Matrix :\n"); 
    printf("\nInput the rows and columns of the matrix : ");
    scanf("%d %d",&r,&c);
    int a[r][c],b[r][c];
    printf("Input elements in the first matrix :\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("element - [%d],[%d] : ",i,j);
            scanf("%d",&a[i][j]);
        }
    } 
    printf("\nThe matrix is :\n");
    for(i=0;i<r;i++)
    {
        printf("\n");
        for(j=0;j<c;j++)
        printf("%d   ",a[i][j]);
    }
    for(i=0;i<r;i++)
    {
    for(j=0;j<c;j++)
        {
               b[j][i]=a[i][j];
        }
    }

    printf("\n\nThe transpose of a matrix is : ");
    for(i=0;i<c;i++)
    {
        printf("\n");
        for(j=0;j<r;j++)
        {
            printf("%d   ",b[i][j]);
        }
    }
    printf("\n");
	return 0;
}