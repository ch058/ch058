#include <stdio.h>

struct fraction
{
    int numerator, denominator;
};

void input(struct fraction *a , int n)
{
    for(int i=0;i<n;i++)
    {
        printf("\nEnter details for fraction %d\n",(i+1));
        printf("\nEnter numerator : ");
        scanf("%d",&a[i].numerator);
        printf("\nEnter denominator : ");
        scanf("%d",&a[i].denominator);
    }
}

int gcfCalc(int a, int b)
{
    int diff=a;
    while(a!=b)
    {
        if(a>b)
        {
            a-=b;
            diff=a;
        }
        else
        {
            b-=a;
            diff=b;
        }
    }
    return diff;
}

struct fraction addFrac(struct fraction x[], int n)
{
    struct fraction sum=x[0];
    for(int i=1;i<n;i++)
    {
        sum.numerator = sum.numerator*x[i].denominator + x[i].numerator*sum.denominator;
        sum.denominator*=x[i].denominator;
        int gcf = gcfCalc(sum.numerator,sum.denominator);
        sum.numerator/=gcf;
        sum.denominator/=gcf;
    }
    return sum;
}

void output(struct fraction a[], int n, struct fraction sum)
{
    printf("\nThe sum of fractions\n\n");
    for(int i=0;i<n-1;i++)
    {
        printf("    %d/%d\n",a[i].numerator,a[i].denominator);
    }
    printf("and %d/%d \n\nis  %d/%d",a[n-1].numerator,a[n-1].denominator,sum.numerator,sum.denominator);
}

int main()
{
    printf("Enter the number of fractions to be added : ");
    int n;
    scanf("%d",&n);
    struct fraction a[n],sum;
    input(a,n);
    sum = addFrac(a,n);
    output(a,n,sum);
    return 0;
}