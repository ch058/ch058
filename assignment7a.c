#include <stdio.h>

int input()
{
    int n;
    printf("Enter a number : ");
    scanf("%d",&n);
    return n;
}
void printPattern(int n)
{
    for(int i=1 ; i<=n ; i++)
    {
        for(int j=1 ; j <= i ; j++)
            printf("%d",j);
        printf("\n");
    }
}

int main()
{
    printPattern(input());
    return 0;
}