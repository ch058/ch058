#include <stdio.h>

struct string
{
    char str[100];
};

struct string input()
{
    struct string s;
    printf("\nEnter a string : ");
    scanf("%[^\n]%*c", s.str);
	return s;
}
void lower(struct string *s)
{
    for(int i=0;s->str[i]!='\0';i++)
        if(s->str[i]>='A'&&s->str[i]<='Z')
            s->str[i]+=32;
}

void printString(struct string str)
{
    printf("%s",str.str);
}

int main()
{
    struct string s = input();
    lower(&s);
    printf("\nThe string converted to lowercase alphabets is : ");
    printString(s);
}