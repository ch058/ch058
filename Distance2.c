#include<stdio.h>
#include<math.h>

struct point
{
    float x,y;
};

void input(struct point *a, struct point *b)
{
    printf("Enter details for 1st point\n\n");
    printf("\nEnter x coordinate : ");
    scanf("%f", &a->x);
    printf("\nEnter y coordinate : ");
    scanf("%f", &a->y);
    printf("Enter details for 2nd point\n\n");
    printf("\nEnter x coordinate : ");
    scanf("%f", &b->x);
    printf("\nEnter y coordinate : ");
    scanf("%f", &b->y);
    
}

float distCalc(struct point a , struct point b)
{
    float distance = sqrt(pow((a.x-b.x),2)+pow((a.y-b.y),2));
    return distance;
}

void printCoord(struct point a)
{
    printf("\nX coordinate = %f\n",a.x);
    printf("Y coordinate = %f\n",a.y);
}

void output(struct point a , struct point b, float distance)
{
    printf("\nFirst point: \n");
	printCoord(a);
	printf("\nSecond point: \n");
	printCoord(b);
	printf("\nDistance between the points: %f \n",distance);
}

int main()
{
	printf("Enter the coordinates of the points\n\n");
	struct point a,b;
	input(&a,&b);
	float distance = distCalc(a,b);
	output(a,b,distance);
	return 0;
}