#include <stdio.h>

struct string
{
    char str[100];
};

struct string input()
{
    struct string s;
    printf("\nEnter a string : ");
    scanf("%[^\n]%*c", s.str);
    return s;
}

struct string concatenate(struct string a , struct string b)
{
    struct string c;
    int i=0, cpos = 0;
    for(i=0; a.str[i]!='\0'; i++, cpos++)
        c.str[cpos]=a.str[i];
    for(i=0; b.str[i]!='\0'; i++, cpos++)
        c.str[cpos]=b.str[i];
    return c;
}

void printString(struct string str)
{
    printf("%s",str.str);
}

int main()
{
    struct string a = input();
    struct string b = input();
    struct string c = concatenate(a,b);
    printf("\nThe first string was : ");
    printString(a);
    printf("\nThe second string was : ");
    printString(b);
    printf("\nThe concatenated string is : ");
    printString(c);
}