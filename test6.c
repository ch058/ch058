#include <stdio.h>

struct string
{
    char ch[50];
};

struct string input()
{
    struct string s;
    printf("Enter the string to be checked for palindrome : ");
    scanf("%s", s.ch);
    return s;
}

int length (struct string s)
{
    int length = 0;
    for(int i=0 ; s.ch[i]!='\0' ; i++)
    {
        length++;
    }
    return length;
}

struct string strReverse(struct string str)
{
    int len = length(str);
    struct string rev;
    for(int i=0;i<len;i++)
    {
        rev.ch[i]=str.ch[len-1-i];
    }
    return rev;
}

int palinCheck(struct string str)
{
    struct string rev = strReverse(str);
    int strlength = length(str);
    int revlength = length(rev);
    if(strlength != revlength)
        return 0;
    int ntcmp = 0;
    for(int i=0; i<strlength; i++)
        if (str.ch[i]!=rev.ch[i])
            return 0;
    return 1;
}

void printPalincheck(struct string str)
{
    if(palinCheck(str) == 1)
        printf("The string is a palindrome.");
    else
        printf("The string is not a palindrome.");
}

int main()
{
    struct string str = input();
    printPalincheck(str);
    return 0;
}