#include <stdio.h>

int input()
{
    int n;
    printf("Enter a number : ");
    scanf("%d",&n);
    return n;
}
void printPattern(int n)
{
    for(int i=1 ; i<n ; i++)
    {
        for(int j=0 ; j <n ; j++)
            printf("@");
        printf("\n");
    }
}

int main()
{
    printPattern(input());
    return 0;
}